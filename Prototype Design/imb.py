import ibmiotf.device
import time
import random
from pynput import keyboard
from datetime import datetime
import RPi.GPIO as GPIO

#IBM Bluemix Details
orgId = "4c1rpi"
deviceType = "UserDevice"
deviceId = "UD1"
authToken = "ud1prototype"

scanned = "unscanned"
occupied = "occupied"

SENSOR_PIN = 8

#Connection Details
options = {"org": orgId, "type": deviceType, "id": deviceId, "auth-method": "token" , "auth-token": authToken}
client = ibmiotf.device.HttpClient(options)

#GPIO Setup
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(SENSOR_PIN, GPIO.IN) 

def get_time():
    now = datetime.now()
    return str(now.strftime("%d/%m/%Y %H:%M:%S"))

def on_press(key):
    global scanned
    
    if key == keyboard.Key.space:
        scanned = "scanned"
        print("QR code scanned")
        
    elif key == keyboard.Key.ctrl:
        scanned = "unscanned"
        print("QR code scanned out")

#Start keyboard listener
listener = keyboard.Listener(on_press=on_press)
listener.start()

#Data Publisher
while True:
    if GPIO.input(SENSOR_PIN) == 1:
        occupied = "not occupied"
    else:
        occupied = "taken"
        
    data = {'room_id': 'B2.1', 'scanned': scanned, 'occupied': occupied, 'time': get_time()}
    client.publishEvent("test", "json", data)
    
    print(data)
    time.sleep(5)
            

